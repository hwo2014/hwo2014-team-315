import json
import socket
import sys
import ai
import path
import track

class NoobBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.__class__.name = name
        self.name = name
        self.key = key
        self.acc = 0.6
        self.angle = 0.0
        self.ai = ai.Brain(self)  #  ai.NoobAI()
        self.pieces = []
        self.color = "unknown"
        self.tick = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    # {"msgType": "yourCar", "data": {
    #     "name": "Schumacher",
    #     "color": "red"
    # }}
    def on_your_car(self, data):
        self.color = data['color']
        print("Your car: {}".format(self.color))
        self.ping()

    def on_game_init(self, data):
        # Not sure what
        self.track = track.Track(data['race']['track'])
        # self.pieces = path.Path.parse(data['race']['track']['pieces'])
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data, tick):
        # relevant = filter(lambda (x): x['id']['name'] == self.name, data)[0]
        # relevant_piece = self.pieces[relevant['piecePosition']['pieceIndex']]
        # acc = self.ai.on_car_position(relevant, relevant_piece, tick)
        acc = self.ai.on_car_position(data, tick)
        self.throttle(acc)

    def on_crash(self, data, tick):
        self.ai.on_crash(data, tick)
        self.ping()

    def on_lap_finished(self, data):
        print ("Lap time {0}".format(data['lapTime']))
        print ("Race time {0}".format(data['raceTime']))

    def on_game_end(self, data):
        print("Race ended {0}".format(data))
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'lapFinished': self.on_lap_finished,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type == 'carPositions':
                gameTick = 0
                if ('gameTick' in msg.keys()):
                    gameTick = msg['gameTick']
                self.on_car_positions(data, gameTick)
            elif msg_type == 'crash':
                self.on_crash(data, gameTick)
            elif msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        #print("Usage: ./run host port botname botkey")
        host = "testserver.helloworldopen.com"
        port = 8091
        name = "Lamenso"
        key = "0XIVUiq97ZgTZg"
    else:
        host, port, name, key = sys.argv[1:5]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(host, port, name, key))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = NoobBot(s, name, key)
    bot.run()
