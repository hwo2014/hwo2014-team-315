import math


class Track(object):
    lanes = []
    pieces = []

    def __init__(self, d):
        Track.lanes = [l['distanceFromCenter'] for l in d['lanes']]
        Track.pieces = [Piece(p) for p in d['pieces']]


class Piece(object):
    def __init__(self, d):
        self.switch = d['switch'] if 'switch' in d.keys() else False
        self.radius = d['radius'] if 'radius' in d.keys() else 1000.0
        self.angle = d['angle'] if 'angle' in d.keys() else 0.0
        self.is_turn = (self.angle != 0.0)
        self.is_clockwise = (self.angle > 0.0)

        nsign = 1
        math.copysign(nsign, self.angle)
        nsign *= -1

        if self.is_turn:
            self.length = [(self.radius + nsign * li) * math.pi * self.angle / 180.0 for li in Track.lanes]
        else:
            self.length = [d['length']]

    def len(self, li):
        return self.length[li] if self.is_turn else self.length[0]
