import main


class NoobAI:
    def __init__(self):
        self.position = {
            'piecePosition': {
                'inPieceDistance': 0
            }
        }
        self.piece = 0
        self.angle = 0
        self.tick = 0
        self.speed = 0.6
        self.acc = 0.6

    def on_car_position(self, position, piece, tick):
        # Step 1. Already processed following tick
        if (self.tick >= tick):
            return self.acc
        # Step 2. Update values
        speed_before = self.speed
        acc_before = self.acc
        # Step 2.2. Calculating speed
        old_piece_distance = self.position['piecePosition']['inPieceDistance']
        new_piece_distance = position['piecePosition']['inPieceDistance']
        # Step 2.3. Checking if piece changed
        if (new_piece_distance < old_piece_distance):
            old_piece_distance = - piece.edge_distance(old_piece_distance)
            self.piece = piece
        self.speed = (new_piece_distance - old_piece_distance) / (tick - self.tick)
        if (self.speed < 0):
            print ("NS piece {0} to {1}".format(self.piece, piece))
            print ("NS position {0} to {1}".format(self.position, position))
        # Step 2.4. Updating data
        piece.trace(speed_before, self.speed, acc_before)
        self.position = position
        self.angle = position['angle']
        self.tick = tick
        self.acc = piece.optimal_acc(self.speed, self.angle)
        return self.acc

    def on_crash(self, data):
        print("Crashed throttle={0}, angle={1}, speed={2}".format(self.acc, ))
        self.acc = 0.6


#################################### AI engine 2 #####################################

class Brain(object):
    def __init__(self, master):
        self.m = master
        self.old_throttle = 0.655
        self.throttle = 0.655
        self.old_speed = 0.0
        self.speed = 0.0
        self.old_angle = 0.0
        self.angle = 0.0
        self.old_tick = 0
        self.tick = -1
        self.old_lane = 0
        self.lane = 0
        self.old_piece = 0
        self.piece = 0
        self.old_pos = 0.0
        self.pos = 0.0
        self.dist = 0.0

    # "id": {
    #   "name": "Schumacher",
    #   "color": "red"
    # },
    # "angle": 0.0,
    # "piecePosition": {
    #   "pieceIndex": 0,
    #   "inPieceDistance": 0.0,
    #   "lane": {
    #     "startLaneIndex": 0,
    #     "endLaneIndex": 0
    #   },
    #   "lap": 0
    # }
    def on_car_position(self, data, tick):
        my = [d for d in data if d['id']['name'] == self.m.name][0]
        self.old_tick = self.tick
        self.tick = tick
        self.old_angle = self.angle
        self.angle = my['angle']
        pos = my['piecePosition']
        self.old_lane = pos['lane']['startLaneIndex']
        self.lane = pos['lane']['endLaneIndex']
        self.old_piece = self.piece
        self.piece = pos['pieceIndex']
        self.old_pos = self.pos
        self.pos = pos['inPieceDistance']

        self.dist = (self.m.track.pieces[self.old_piece].len(self.old_lane) - self.old_pos) + self.pos \
            if self.old_piece != self.piece \
            else self.pos - self.old_pos
        self.old_speed = self.speed
        self.speed = self.dist / (self.tick - self.old_tick)

        if tick % 20 == 0:
            print("tick={0}, angle={1}, piece={2}, pos={3}, speed={4}".format(tick, self.angle, self.piece, self.pos, self.speed))

        return self.throttle

    def on_crash(self, data, tick):
        print("Crashed :( ->\n->tick={0}, angle={1}, piece={2}, pos={3}, speed={4}".
              format(tick, self.angle, self.piece, self.pos, self.speed))
        self.throttle = 0.655
