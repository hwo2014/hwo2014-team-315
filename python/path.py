import math

class Path(object):
    def __init__(self, idx):
        self.idx = idx
        self.next = 0
        self.friction = 0

    def idx(self):
        return self.idx

    @staticmethod
    def parse(pieces):
        paths = map(lambda (idx, piece): Path.construct(idx, piece), enumerate(pieces))
        for path in paths:
            next = (path.idx + 1) % len(paths)
            path.set_next(paths[next])
        return paths

    @staticmethod
    def construct(idx, piece):
        if 'length' in piece.keys():
            return StraightPath(idx, piece['length'])
        else:
            return CurlPath(idx, piece['radius'], piece['angle'])

    def trace(self, speed_before, speed_after, acc_before):
        self.friction = acc_before - (speed_after - speed_before)
        print("{0} > {1} estimated friction sb: {2:.2g} / sa: {3:.2g} / acc: {4:.2g} / frc: {5:.2g}".format(self.__class__.__name__, self.idx, speed_before, speed_after, acc_before, self.friction))

    def set_next(self, next):
        self.next = next

class StraightPath(Path):
    def __init__(self, idx, length):
        Path.__init__(self, idx)
        self.length = length

    def optimal_acc(self, speed, angle):
        toOptimal = (self.optimal_speed() - speed) + self.friction
        if (toOptimal > 0):
            if(toOptimal > 1.0):
                return 1.0
            else:
                return toOptimal
        return 0

    def optimal_speed(self):
        if(self.next.__class__.__name__ == "StraightPath"):
            return 9.0
        else:
            return 8.0

    def edge_distance(self, old_piece_distance):
        return self.length - old_piece_distance

    def __str__( self ):
        return "Straight length: {0:.3g}".format(self.length)


class CurlPath(Path):
    def __init__(self, idx, radius, angle):
        Path.__init__(self, idx)
        self.radius = radius
        self.angle = angle
        # TODO this formula should be the one for the length, but data returned is inconsistent
        # self.length = (math.pi * angle * radius) / 180
        # Following is just an adjustion
        self.length = math.fabs((90 / angle) * radius)

    def optimal_acc(self, speed, angle):
        toOptimal = (5.3 - speed) + self.friction
        if (toOptimal > 0):
            if(toOptimal > 1.0):
                return 1.0
            else:
                return toOptimal
        return 0

    def edge_distance(self, old_piece_distance):
        return self.length - old_piece_distance

    def __str__( self ):
        return "Curl radius: {0}, angle: {1}, length: {2:.3g}".format(self.radius, self.angle, self.length)